package me.martijnpu.prefix.EventHandler;

import me.martijnpu.prefix.Core;
import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.FileHandler.Permission;
import me.martijnpu.prefix.Util.Color;
import me.martijnpu.prefix.Util.DefaultFontInfo;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.jetbrains.annotations.Nullable;

import static me.martijnpu.prefix.Core.hasColorPermission;

class ListViewer {
    static void showPrefixColorList(@Nullable Object sender) {
        if (!Permission.PREFIX_LIST.hasPermissionMessage(sender))
            return;

        boolean reduceSpaces = sender == null;
        TextComponent colorList = getTitle("Prefix   |   Name", reduceSpaces);
        TextComponent separator = new TextComponent("  |  ");
        separator.setColor(ChatColor.GRAY);

        for (Color color : Color.values()) {
            if (color == Color.HEXADECIMAL)
                continue;

            TextComponent nameColor = checkColor(sender, color, "name");
            TextComponent prefixColor = checkColor(sender, color, "prefix");

            TextComponent colorLine = new TextComponent();
            colorLine.addExtra(prefixColor);
            colorLine.addExtra(separator);
            colorLine.addExtra(nameColor);
            colorList.addExtra("\n" + DefaultFontInfo.adaptSpaces(ChatColor.stripColor(colorLine.toPlainText()), false, false, reduceSpaces));
            colorList.addExtra(colorLine);
        }
        if (Permission.PREFIX_CHAR.hasPermission(sender))
            colorList.addExtra(getCharLine(reduceSpaces));

        Messages.LIST.sendBig(sender, colorList);
    }

    static void showSuffixColorList(@Nullable Object sender) {
        if (!Permission.SUFFIX_LIST.hasPermissionMessage(sender))
            return;

        boolean reduceSpaces = sender == null;
        TextComponent colorList = getTitle("Suffix", reduceSpaces);

        for (Color color : Color.values()) {
            if (color == Color.HEXADECIMAL)
                continue;

            TextComponent suffixColor = checkColor(sender, color, "suffix");

            colorList.addExtra("\n" + DefaultFontInfo.adaptSpaces(ChatColor.stripColor(suffixColor.toPlainText()), false, false, reduceSpaces));
            colorList.addExtra(suffixColor);
        }
        if (Permission.PREFIX_CHAR.hasPermission(sender))
            colorList.addExtra(getCharLine(reduceSpaces));

        Messages.LIST.sendBig(sender, colorList);
    }

    static void showTemplateList(Object sender) {
        if (!Permission.TEMPLATE_LIST.hasPermissionMessage(sender))
            return;
        if (!Core.hasAnTemplatePermission(sender)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        TextComponent templateList = new TextComponent(Messages.TEMPLATE_LISTHEADER.convert());

        for (String template : ConfigData.TEMPLATE_LIST.get()) {
            if (Core.hasTemplatePermission(sender, template)) {
                TextComponent templateComponent = new TextComponent("\n" + template);
                templateComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST.convert("&fClick to apply &b" + template)));
                templateComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/prefixtemplate " + template));
                templateList.addExtra(templateComponent);
            }
        }

        Messages.LIST.sendBig(sender, templateList);
    }

    private static TextComponent getTitle(String text, boolean reduceSpaces) {
        text = "&7&l" + text;
        return new TextComponent(Messages.LIST.convert(DefaultFontInfo.adaptSpaces(text, true, true, reduceSpaces)));
    }

    private static TextComponent checkColor(Object sender, Color color, String type) {
        TextComponent nameColor = new TextComponent(Messages.LIST.convert(color.getColorString("")));
        Permission permission;
        String cmd;

        switch (type) {
            case "prefix":
                permission = Permission.COLOR_BASE_PREFIX;
                cmd = "/prefix color " + color.getName();
                break;
            case "suffix":
                permission = Permission.COLOR_BASE_SUFFIX;
                cmd = "/suffix color " + color.getName();
                break;
            case "name":
                permission = Permission.COLOR_BASE_NAME;
                cmd = "/prefix name " + color.getName();
                break;
            default:
                permission = Permission.COLOR_BASE;
                cmd = "/command unknown";
                break;
        }

        if (sender == null || hasColorPermission(sender, color, permission)) {
            nameColor.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd));
            nameColor.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST_APPLY.convert(type)));
        } else {
            nameColor.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST_NO_PERM.convert(type)));
            nameColor.setStrikethrough(true);
        }
        return nameColor;
    }

    private static TextComponent getCharLine(boolean reduceSpaces) {
        TextComponent color = new TextComponent("\n" + DefaultFontInfo.adaptSpaces("&&", true, false, reduceSpaces));
        color.setColor(ChatColor.GOLD);
        color.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST.convert("&fYou can use colorcodes")));

        if (ConfigData.HEX_ENABLED.get()) {
            TextComponent hex = new TextComponent("\n" + DefaultFontInfo.adaptSpaces(ConfigData.HEX_FORMAT.get(), true, false, reduceSpaces));
            hex.setColor(ChatColor.GOLD);
            hex.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST.convert("&fYou can use hexadecimal")));
            color.addExtra(hex);
        }
        return color;
    }
}