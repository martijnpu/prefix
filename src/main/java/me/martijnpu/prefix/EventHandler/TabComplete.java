package me.martijnpu.prefix.EventHandler;

import me.martijnpu.prefix.Core;
import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Permission;
import me.martijnpu.prefix.Util.Color;
import me.martijnpu.prefix.Util.HexColor;
import me.martijnpu.prefix.Util.Statics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static me.martijnpu.prefix.Util.Statics.debug;
import static me.martijnpu.prefix.Util.Statics.getOnlinePlayers;

public class TabComplete {
    public List<String> onSuffixTabComplete(Object sender, String[] args) {
        List<String> list = new ArrayList<>();

        if (args.length == 1) {
            list.add("list");
            list.add("help");
            if (Permission.SUFFIX_CHANGE.hasPermission(sender))
                list.add("reset");
            if (Core.hasAnColorPermission(sender, Permission.COLOR_BASE_SUFFIX))
                list.add("color");
            if (Permission.ADMIN.hasPermission(sender)) {
                if (debug)
                    list.add("debug");
            }
            if (Permission.SUFFIX_REMOVE.hasPermission(sender))
                list.add("remove");
            return sort(list, args);
        }

        switch (args[0].toLowerCase()) {
            case "list":
            case "help":
                break;

            case "color":
                if (!Core.hasAnColorPermission(sender, Permission.COLOR_BASE_SUFFIX))
                    break;
                if (args.length == 2)
                    list.addAll(getColorPerms(sender, Permission.COLOR_BASE_SUFFIX));
                if (args.length == 3 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "reset":
                if (!Permission.SUFFIX_CHANGE.hasPermission(sender))
                    break;
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "remove":
                if (!Permission.SUFFIX_REMOVE.hasPermission(sender))
                    break;
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            default:
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(Statics.getOnlinePlayers());
                break;
        }
        return sort(list, args);
    }

    public List<String> onPrefixTabComplete(Object sender, String[] args) {
        List<String> list = new ArrayList<>();

        if (args.length == 1) {
            list.add("list");
            list.add("help");
            if (Permission.PREFIX_CHANGE.hasPermission(sender))
                list.add("reset");
            if (Core.hasAnColorPermission(sender, Permission.COLOR_BASE_PREFIX))
                list.add("color");
            if (Core.hasAnColorPermission(sender, Permission.COLOR_BASE_NAME))
                list.add("name");
            if (ConfigData.PREFIX_BRACKET_ENABLED.get() && ConfigData.PREFIX_BRACKET_COLOR.get().isEmpty() && Core.hasAnColorPermission(sender, Permission.COLOR_BASE_BRACKET))
                list.add("bracket");
            if (ConfigData.TEMPLATE_ENABLED.get() && Core.hasAnTemplatePermission(sender))
                list.add("template");
            if (Permission.ADMIN.hasPermission(sender)) {
                list.add("reload");
                list.add("version");
                if (debug)
                    list.add("debug");
            }
            return sort(list, args);
        }

        switch (args[0].toLowerCase()) {
            case "list":
            case "help":
                break;

            case "color":
                if (!Core.hasAnColorPermission(sender, Permission.COLOR_BASE_PREFIX))
                    break;
                if (args.length == 2)
                    list.addAll(getColorPerms(sender, Permission.COLOR_BASE_PREFIX));
                if (args.length == 3 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "name":
                if (!Core.hasAnColorPermission(sender, Permission.COLOR_BASE_NAME))
                    break;
                if (args.length == 2)
                    list.addAll(getColorPerms(sender, Permission.COLOR_BASE_NAME));
                if (args.length == 3 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "bracket":
                if (!ConfigData.PREFIX_BRACKET_ENABLED.get() || !ConfigData.PREFIX_BRACKET_ENABLED.get() || !ConfigData.PREFIX_BRACKET_COLOR.get().isEmpty())
                    break;
                if (args.length == 2)
                    list.addAll(getColorPerms(sender, Permission.COLOR_BASE_BRACKET));
                if (args.length == 3 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "reset":
                if (!Permission.PREFIX_CHANGE.hasPermission(sender))
                    break;
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "template":
                args = Arrays.copyOfRange(args, 1, args.length);
                list.addAll(onTemplateTabComplete(sender, args));
                break;

            default:
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(Statics.getOnlinePlayers());
                break;
        }
        return sort(list, args);
    }

    public List<String> onTemplateTabComplete(Object sender, String[] args) {
        List<String> list = new ArrayList<>();

        if (!ConfigData.TEMPLATE_ENABLED.get() || !Core.hasAnTemplatePermission(sender))
            return list;

        if (args.length == 1) {
            list.addAll(getAvailableTemplates(sender));
            return sort(list, args);
        }

        switch (args[0].toLowerCase()) {
            case "list":
                break;

            case "reset":
            default:
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;
        }
        return sort(list, args);
    }

    private List<String> sort(List<String> list, String[] args) {
        if (args.length == 5)
            list.add("Yeeeeeeet"); //EasterEgg
        else if (args.length == 10)
            list.add(">;D"); //EasterEgg

        list.removeIf((x) -> !x.toLowerCase().startsWith(args[args.length - 1].toLowerCase()));
        Collections.sort(list);
        return list;
    }

    private List<String> getColorPerms(Object sender, Permission permission) {
        try {
            List<String> result = new ArrayList<>();
            for (Color c : Color.values())
                if (c != Color.HEXADECIMAL && Core.hasColorPermission(sender, c, permission))
                    result.add(c.getName());

            if (ConfigData.HEX_ENABLED.get()) {
                result.add(ConfigData.HEX_FORMAT.get());
                if (ConfigData.HEX_NAMES.get())
                    for (HexColor hex : HexColor.values())
                        result.add(hex.getName());
            }

            if (!result.isEmpty())
                result.add("reset");
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    private List<String> getAvailableTemplates(Object sender) {
        try {
            List<String> result = new ArrayList<>();

            for (String template : ConfigData.TEMPLATE_LIST.get())
                if (Core.hasTemplatePermission(sender, template))
                    result.add(template);

            if (!result.isEmpty()) {
                result.add("reset");
                result.add("list");
            }
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }
}
