package me.martijnpu.prefix.Paper;

import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Interfaces.IConfig;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PaperFileManager extends IConfig {
    private static final PaperFileManager instance = new PaperFileManager();
    private YamlConfiguration messagesConfig = null;

    private PaperFileManager() {
        reload();
    }

    public static PaperFileManager getInstance() {
        return instance;
    }

    public void printInitData() {
        Messages.PLUGIN.sendConsole("&aLoading " + Messages.values().length + " messages.");

        for (Messages message : Messages.values())
            if (!messagesConfig.contains(message.getPath()) && !message.getPath().equalsIgnoreCase("custom-does-not-exist"))
                Messages.WARN.sendConsole("&cMissing message &b\"" + message.getPath() + "\"&c Regenerate the file or copy the default from spigot");
    }

    public String getMessage(String path) {
        if (messagesConfig == null)
            return path;
        return messagesConfig.getString(path, path);
    }

    protected File messagesFile() {
        return new File(Main.get().getDataFolder(), "languages/" + locale + ".yml");
    }

    /**
     * During the default load no messages will be displayed since you're loading the English text.
     * Afterward the custom language file will be loaded if possible
     */
    protected void loadMessagesConfig(boolean loadDefault) {
        locale = loadDefault ? "en_US" : ConfigData.LOCALE.get();

        if (!messagesFile().exists() && Main.get().getResource("languages/" + locale + ".yml") == null) {
            Messages.WARN.sendConsole("Language '" + locale + "' does not exist. Switching back to English!");
            locale = "en_US";
        }

        if (!messagesFile().exists())
            Main.get().saveResource("languages/" + locale + ".yml", false);
        messagesConfig = new YamlConfiguration();
        messagesConfig.options().parseComments(true);

        try {
            messagesConfig.load(messagesFile());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!loadDefault) {
            Messages.PLUGIN.sendConsole("&aLoading language pack: &3" + getMessage(Messages.LANGUAGE.getPath()));

            for (String languageFile : getResourceFiles()) {
                if (!new File(Main.get().getDataFolder(), languageFile).exists())
                    Main.get().saveResource(languageFile, false);
            }
        }
    }

    protected void reloadConfig() {
        Main.get().saveDefaultConfig();
        Main.get().reloadConfig();
    }

    @Override
    public boolean getBoolean(String path, boolean def) {
        return Main.get().getConfig().getBoolean(path, def);
    }

    @Override
    public String getString(String path, String def) {
        return Main.get().getConfig().getString(path, def);
    }

    @Override
    public int getInt(String path, int def) {
        return Main.get().getConfig().getInt(path, def);
    }

    @Override
    public List<String> getStringList(String path, List<String> def) {
        return Main.get().getConfig().getStringList(path);
    }

    @Override
    public Set<String> getConfigKeyList(String path, Set<String> def) {
        ConfigurationSection cfg = Main.get().getConfig().getConfigurationSection(path);
        return cfg == null ? new HashSet<>() : cfg.getKeys(false);
    }

    @Override
    public boolean isSet(String path) {
        return Main.get().getConfig().isSet(path);
    }

    @Override
    public void set(String path, Object value) {
        Main.get().getConfig().set(path, value);
    }

    @Override
    public void saveConfig() {
        Main.get().saveConfig();
    }

    @Override
    public void reload() {
        loadMessagesConfig(true);
        reloadConfig();
    }

    @Override
    public void loadLocale() {
        loadMessagesConfig(false);
    }
}
