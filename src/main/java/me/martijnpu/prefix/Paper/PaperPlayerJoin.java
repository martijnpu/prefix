package me.martijnpu.prefix.Paper;

import me.martijnpu.prefix.Util.Statics;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PaperPlayerJoin implements Listener {
    PaperPlayerJoin() {
        Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Statics.onPlayerJoin(e.getPlayer());
    }
}
