package me.martijnpu.prefix.Paper;

import me.martijnpu.prefix.Util.Interfaces.ICommand;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

class PaperCommand extends ICommand implements CommandExecutor, TabCompleter {
    PaperCommand() {
        PluginCommand commandPrefiX = registerCommand("prefix", "tag");
        commandPrefiX.setExecutor(this);
        commandPrefiX.setTabCompleter(this);

        PluginCommand commandSuffiX = PaperCommand.registerCommand("suffix");
        commandSuffiX.setExecutor(this);
        commandSuffiX.setTabCompleter(this);

        PluginCommand commandTemplate = PaperCommand.registerCommand("prefixtemplate", "prefixtemp");
        commandTemplate.setExecutor(this);
        commandTemplate.setTabCompleter(this);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        if (cmd.getName().equalsIgnoreCase("prefix"))
            return onPrefixTabComplete(sender, args);
        if (cmd.getName().equalsIgnoreCase("suffix"))
            return onSuffixTabComplete(sender, args);
        if (cmd.getName().equalsIgnoreCase("prefixtemplate"))
            return onTemplateTabComplete(sender, args);
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (cmd.getName().equalsIgnoreCase("prefix"))
            return onPrefixCommand(sender, (sender instanceof Player), args);
        if (cmd.getName().equalsIgnoreCase("suffix"))
            return onSuffixCommand(sender, (sender instanceof Player), args);
        if (cmd.getName().equalsIgnoreCase("prefixtemplate"))
            return onTemplateCommand(sender, (sender instanceof Player), args);
        return false;
    }

    static PluginCommand registerCommand(String... aliases) {
        PluginCommand command = getCommand(aliases[0], Main.get());

        command.setAliases(Arrays.asList(aliases));
        getCommandMap().register(Main.get().getDescription().getName(), command);
        return command;
    }

    private static PluginCommand getCommand(String name, Plugin plugin) {
        PluginCommand command = null;

        try {
            Constructor<PluginCommand> c = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            c.setAccessible(true);

            command = c.newInstance(name, plugin);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | InstantiationException |
                 InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return command;
    }

    private static CommandMap getCommandMap() {
        CommandMap commandMap = null;

        try {
            if (Main.get().getServer().getPluginManager() instanceof SimplePluginManager) {
                Field f = SimplePluginManager.class.getDeclaredField("commandMap");
                f.setAccessible(true);

                commandMap = (CommandMap) f.get(Main.get().getServer().getPluginManager());
            }
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return commandMap;
    }
}
