package me.martijnpu.prefix.Bungee;

import me.martijnpu.prefix.Util.Interfaces.IStatic;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class BungeeStatics implements IStatic {
    private static final BungeeStatics instance = new BungeeStatics();

    private BungeeStatics() {
    }

    public static BungeeStatics getInstance() {
        return instance;
    }

    @Override
    public String getServerVersion() {
        return ProxyServer.getInstance().getVersion();
    }

    @Override
    public List<String> getOnlinePlayers() {
        return ProxyServer.getInstance().getPlayers().stream().map(CommandSender::getName).collect(Collectors.toList());
    }

    @Override
    public String getDisplayName(Object player) {
        if (player instanceof ProxiedPlayer)
            return ((ProxiedPlayer) player).getDisplayName();
        else
            return "Console";
    }

    @Override
    public UUID getUUID(Object sender) {
        if (sender instanceof ProxiedPlayer)
            return ((ProxiedPlayer) sender).getUniqueId();
        else
            return null;
    }

    @Override
    public Object getPlayer(String name) {
        return ProxyServer.getInstance().getPlayers().stream().filter(i -> i.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }
}
