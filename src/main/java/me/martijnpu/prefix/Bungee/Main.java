package me.martijnpu.prefix.Bungee;

import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.LuckPermsConnector;
import me.martijnpu.prefix.Util.Interfaces.*;
import me.martijnpu.prefix.Util.Statics;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import org.bstats.bungeecord.Metrics;
import org.bstats.charts.SimplePie;

import java.util.concurrent.TimeUnit;

public class Main extends Plugin implements IPrefix {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Statics.isProxy = true;
        PrefixAdapter.setAdapter(this);
        Statics.setCurrVersion(getDescription().getVersion());

        if (LuckPermsConnector.checkInvalidVersion()) {
            this.onDisable();
            return;
        }
        ProxyServer.getInstance().getScheduler().schedule(this, this::checkForUpdates, 0, 1, TimeUnit.DAYS);

        BungeeFileManager.getInstance().printInitData();
        ConfigData.getInstance().printInitData();
        new BungeeCommand();
        new BungeePlayerJoin();

        if (ConfigData.BSTATS.get()) {
            Metrics metrics = new Metrics(this, 10921);
            metrics.addCustomChart(new SimplePie("luckpermsVersion", LuckPermsConnector::getLPVersion));
            metrics.addCustomChart(new SimplePie("language", ConfigData.LOCALE::get));
        }

        Messages.PLUGIN.sendConsole("&aWe're up and running");
    }

    @Override
    public void onDisable() {
        Messages.PLUGIN.sendConsole("&aDisabled successful");
    }

    private void checkForUpdates() {
        ProxyServer.getInstance().getScheduler().runAsync(this, Statics::checkForUpdate);
    }

    @Override
    public IMessage getMessagesAdapter() {
        return BungeeMessages.getInstance();
    }

    @Override
    public IConfig getConfigAdapter() {
        return BungeeFileManager.getInstance();
    }

    @Override
    public IStatic getStaticsAdapter() {
        return BungeeStatics.getInstance();
    }
}
