package me.martijnpu.prefix.Bungee;

import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Interfaces.IMessage;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeeMessages implements IMessage {
    private static final BungeeMessages instance = new BungeeMessages();

    private BungeeMessages() {
    }

    public static BungeeMessages getInstance() {
        return instance;
    }

    private String colorMessage(String message) {
        if (message.isEmpty()) return "";
        return ChatColor.translateAlternateColorCodes('&', BungeeFileManager.getInstance().getMessage(Messages.PREFIX.getPath()) + message);
    }

    @Override
    public void sendConsoleWarning(String message) {
        Main.get().getLogger().warning(ChatColor.translateAlternateColorCodes('&', message));
    }

    @Override
    public void sendConsole(String message) {
        Main.get().getLogger().info(ChatColor.translateAlternateColorCodes('&', message));
    }

    @Override
    public String getMessage(String path) {
        return BungeeFileManager.getInstance().getMessage(path);
    }

    @Override
    public void sendBigMessage(Object sender, TextComponent message) {
        if (sender instanceof ProxiedPlayer)
            ((ProxiedPlayer) sender).sendMessage(message);
        else
            sendConsole(TextComponent.toLegacyText(message));
    }

    @Override
    public void sendMessage(Object sender, String message) {
        if (sender instanceof ProxiedPlayer)
            ((ProxiedPlayer) sender).sendMessage(TextComponent.fromLegacyText(colorMessage(message)));
        else
            sendConsole(message);
    }

    @Override
    public BaseComponent[] convert(String old) {
        return TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', old));
    }
}
