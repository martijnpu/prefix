package me.martijnpu.prefix.Bungee;

import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Interfaces.IConfig;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class BungeeFileManager extends IConfig {
    private static final BungeeFileManager instance = new BungeeFileManager();
    private final File configFile = new File(Main.get().getDataFolder(), "config.yml");
    private Configuration configuration;
    private Configuration messagesConfiguration;

    private BungeeFileManager() {
        reload();
    }

    public static BungeeFileManager getInstance() {
        return instance;
    }

    protected void printInitData() {
        Messages.PLUGIN.sendConsole("&aLoading " + Messages.values().length + " messages.");

        for (Messages message : Messages.values())
            if (!messagesConfiguration.contains(message.getPath()) && !message.getPath().equalsIgnoreCase("custom-does-not-exist"))
                Messages.WARN.sendConsole("&cMissing message &b\"" + message.getPath() + "\"&c Regenerate the file or copy the default from spigot");
    }

    //region Messages
    String getMessage(String path) {
        if (messagesConfiguration == null)
            return path;
        return messagesConfiguration.getString(path, path);
    }

    protected File messagesFile() {
        return new File(Main.get().getDataFolder(), "languages/" + locale + ".yml");
    }

    private void loadMessagesConfig(boolean loadDefault) {
        try {
            locale = loadDefault ? "en_US" : ConfigData.LOCALE.get();
            if (!Main.get().getDataFolder().exists())
                Main.get().getDataFolder().mkdir();
            if (!messagesFile().getParentFile().exists())
                messagesFile().getParentFile().mkdir();

            if (!messagesFile().exists() && Main.get().getResourceAsStream("languages/" + locale + ".yml") == null) {
                Messages.WARN.sendConsole("&cLanguage '&b" + locale + "&c' does not exist. Switching back to English!");
                locale = "en_US";
            }

            if (!messagesFile().exists()) {
                try (InputStream in = Main.get().getResourceAsStream("languages/" + locale + ".yml")) {
                    Files.copy(in, messagesFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            messagesConfiguration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(messagesFile());


            if (!loadDefault) {
                Messages.PLUGIN.sendConsole("&aLoading language pack: &3" + getMessage(Messages.LANGUAGE.getPath()));

                for (String languageFile : getResourceFiles()) {
                    if (!new File(Main.get().getDataFolder(), languageFile).exists())
                        try (InputStream in = Main.get().getResourceAsStream("languages/" + languageFile + ".yml")) {
                            Files.copy(in, messagesFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    //endregion

    //region Config
    private void saveDefaultConfig() {
        if (!Main.get().getDataFolder().exists())
            Main.get().getDataFolder().mkdir();

        if (!configFile.exists()) {
            try (InputStream in = Main.get().getResourceAsStream("config.yml")) {
                Files.copy(in, configFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected void reloadConfig() {
        try {
            saveDefaultConfig();
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean getBoolean(String path, boolean def) {
        return configuration.getBoolean(path, def);
    }

    @Override
    public String getString(String path, String def) {
        return configuration.getString(path, def);
    }

    @Override
    public int getInt(String path, int def) {
        return configuration.getInt(path, def);
    }

    @Override
    public List<String> getStringList(String path, List<String> def) {
        List<String> list = configuration.getStringList(path);
        return list.isEmpty() ? def : list;
    }

    @Override
    public Set<String> getConfigKeyList(String path, Set<String> def) {
        if (!configuration.contains(path))
            return new HashSet<>();
        Configuration cfg = configuration.getSection(path);
        return cfg == null ? new HashSet<>() : (Set<String>) cfg.getKeys();
    }

    @Override
    public boolean isSet(String path) {
        return configuration.contains(path);
    }

    @Override
    public void set(String path, Object value) {
        configuration.set(path, value);
    }

    @Override
    public void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(Main.get().getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reload() {
        loadMessagesConfig(true);
        reloadConfig();
    }

    @Override
    public void loadLocale() {
        loadMessagesConfig(false);
    }

    //endregion
}
