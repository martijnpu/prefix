package me.martijnpu.prefix;

import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.FileHandler.Permission;
import me.martijnpu.prefix.Util.Color;
import me.martijnpu.prefix.Util.Tags.Tag;
import me.martijnpu.prefix.Util.Tags.TagManager;
import net.md_5.bungee.api.ChatColor;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static me.martijnpu.prefix.Util.Statics.getUUID;

public class Core {
    /**
     * Prefix Only
     */
    public static boolean resetPrefixColor(Object sender, Object player) {
        Tag oldTag = TagManager.getTag(player, sender, true);
        if (oldTag == null)
            return false;

        LuckPermsConnector.resetPrefix(getUUID(player));

        Tag newTag = TagManager.getTag(player, sender, true);
        if (newTag == null)
            return false;

        oldTag.setColor(newTag.getNameColor(true), true);

        LuckPermsConnector.setPrefix(getUUID(player), oldTag);
        Messages.COLOR_RESET.send(player, "prefix");
        return true;
    }

    /**
     * Suffix Only
     */
    public static boolean resetSuffixColor(Object sender, Object player) {
        Tag oldTag = TagManager.getTag(player, sender, false);
        if (oldTag == null)
            return false;

        LuckPermsConnector.resetSuffix(getUUID(player));

        Tag newTag = TagManager.getTag(player, sender, false);
        if (newTag == null)
            return false;

        oldTag.setColor(newTag.getNameColor(false), false);

        LuckPermsConnector.setSuffix(getUUID(player), oldTag);
        Messages.COLOR_RESET.send(player, "suffix");
        return true;
    }

    /**
     * Prefix Only
     */
    public static boolean changePrefixColor(Object sender, Object player, Color color, String hexColor) {
        Tag tag = TagManager.getTag(player, sender, true);
        if (tag == null)
            return false;

        tag.changeColor(color, hexColor, true);

        LuckPermsConnector.setPrefix(getUUID(player), tag);
        Messages.COLOR_APPLY.send(player, "prefix", Messages.convertHexadecimalToChat(color.getColorString(hexColor)));
        return true;
    }

    /**
     * Suffix Only
     */
    public static boolean changeSuffixColor(Object sender, Object player, Color color, String hexColor) {
        Tag tag = TagManager.getTag(player, sender, false);
        if (tag == null)
            return false;

        tag.changeColor(color, hexColor, false);

        LuckPermsConnector.setSuffix(getUUID(player), tag);
        Messages.COLOR_APPLY.send(player, "suffix", Messages.convertHexadecimalToChat(color.getColorString(hexColor)));
        return true;
    }

    /**
     * Prefix Only
     */
    public static boolean resetNameColor(Object sender, Object player) {
        Tag oldTag = TagManager.getTag(player, sender, true);
        if (oldTag == null)
            return false;

        LuckPermsConnector.resetPrefix(getUUID(player));

        Tag newTag = TagManager.getTag(player, sender, true);
        if (newTag == null)
            return false;

        oldTag.endColor = newTag.endColor;

        LuckPermsConnector.setPrefix(getUUID(player), oldTag);
        Messages.COLOR_RESET.send(player, "name");
        return true;
    }

    /**
     * Prefix only
     */
    public static boolean changeNameColor(Object sender, Object player, Color color, String hexcolor) {
        Tag tag = TagManager.getTag(player, sender, true);
        if (tag == null)
            return false;

        LuckPermsConnector.resetPrefix(getUUID(player));
        tag.changeEndColor(color, hexcolor);

        LuckPermsConnector.setPrefix(getUUID(player), tag);
        Messages.COLOR_APPLY.send(player, "name", Messages.convertHexadecimalToChat(color.getColorString(hexcolor)));
        return true;
    }

    /**
     * Prefix only
     */
    public static boolean resetBracketColor(Object sender, Object player) {
        Tag oldTag = TagManager.getTag(player, sender, true);
        if (oldTag == null)
            return false;

        LuckPermsConnector.resetPrefix(getUUID(player));

        Tag newTag = TagManager.getTag(player, sender, true);
        if (newTag == null)
            return false;

        oldTag.startColor = newTag.startColor;

        LuckPermsConnector.setPrefix(getUUID(player), oldTag);
        Messages.COLOR_RESET.send(player, "bracket");
        return true;
    }

    /**
     * Prefix only
     */
    public static boolean changeBracketColor(Object sender, Object player, Color color, String hexColor) {
        Tag tag = TagManager.getTag(player, sender, true);
        if (tag == null)
            return false;

        LuckPermsConnector.resetPrefix(getUUID(player));
        tag.changeStartColor(color, hexColor);

        LuckPermsConnector.setPrefix(getUUID(player), tag);
        Messages.COLOR_APPLY.send(player, "bracket", color.getColorString(hexColor));
        return true;
    }

    /**
     * Prefix & Suffix
     */
    public static boolean changePrefix(Object sender, Object player, String prefix) {
        if (TagManager.isInvalidTag(sender, prefix, Permission.PREFIX_CHAR, true))
            return false;
        if (checkIllegalTagText(sender, prefix))
            return false;

        Tag tag = TagManager.getTag(player, sender, true);
        if (tag == null)
            return false;

        tag.tag = prefix;
        LuckPermsConnector.setPrefix(getUUID(player), tag);
        Messages.CMD_CHANGED.send(player, "prefix", Messages.convertHexadecimalToChat(tag.getFullTag()));
        return true;
    }

    /**
     * Suffix Only
     */
    public static boolean changeSuffix(Object sender, Object player, String suffix) {
        if (TagManager.isInvalidTag(sender, suffix, Permission.SUFFIX_CHAR, false))
            return false;
        if (checkIllegalTagText(sender, suffix))
            return false;

        Tag tag = TagManager.getTag(player, sender, false);
        if (tag == null)
            return false;

        /*
        //If the suffix is only used for a custom color
        if(tag.tag.isEmpty() && tag.startChar.isEmpty() && tag.endChar.isEmpty() && ConfigData.SUFFIX_MAX_LENGTH.get() == 0) {
            tag.startColor = "";
            tag.endColor = "";
        }*/

        tag.tag = suffix;
        LuckPermsConnector.setSuffix(getUUID(player), tag);
        Messages.CMD_CHANGED.send(player, "suffix", Messages.convertHexadecimalToChat(tag.getFullTag()));
        return true;
    }

    /**
     * Template Only
     */
    public static boolean changeTemplate(Object sender, Object player, String template) {
        if (ConfigData.TEMPLATE_LIST.get().stream().noneMatch(template::equalsIgnoreCase)) {
            Messages.TEMPLATE_UNKNOWN.send(sender, template);
            return false;
        }

        if (!hasTemplatePermission(sender, template)) {
            Messages.NO_PERM_CMD.send(sender);
            return false;
        }

        String templateContext = ConfigData.getInstance().getCustomString(ConfigData.TEMPLATE_LIST.getPath() + "." + template, "Fail");

        LuckPermsConnector.setPrefix(getUUID(player), templateContext, LuckPermsConnector.getPrefix(getUUID(player)).contextSet);
        Messages.CMD_CHANGED.send(player, "prefix", Messages.convertHexadecimalToChat(templateContext));
        return true;
    }

    /**
     * Prefix Only
     */
    public static void resetPrefix(Object player) {
        LuckPermsConnector.resetPrefix(getUUID(player));
        Messages.CMD_RESET.send(player, "prefix");
    }

    /**
     * Suffix Only
     */
    public static void resetSuffix(Object player) {
        LuckPermsConnector.resetSuffix(getUUID(player));
        Messages.CMD_RESET.send(player, "suffix");
    }

    /**
     * Suffix Only
     */
    public static void removeSuffix(Object player) {
        Tag tag = TagManager.getTag(player, player, false);
        if (tag == null)
            return;

        tag.tag = "";
        LuckPermsConnector.setSuffix(getUUID(player), tag);
        Messages.CMD_REMOVED.send(player, "suffix");
    }

    /**
     * Prefix & Suffix
     */
    public static boolean hasAnColorPermission(Object player, Permission permission) {
        return Arrays.stream(Color.values()).anyMatch(color -> hasColorPermission(player, color, permission));
    }

    /**
     * Prefix & Suffix
     */
    public static boolean hasColorPermission(Object player, Color color, Permission permission) {
        if (player == null) //Assume is console
            return true;
        if (color == Color.HEXADECIMAL && !ConfigData.HEX_ENABLED.get())
            return false;
        return Permission.hasPermission(player, permission.getPath() + "." + color.getName());
    }

    /**
     * Template only
     */
    public static boolean hasAnTemplatePermission(Object player) {
        return ConfigData.TEMPLATE_LIST.get().stream().anyMatch(template -> hasTemplatePermission(player, template));
    }

    /**
     * Template only
     */
    public static boolean hasTemplatePermission(Object player, String template) {
        if (player == null) //Assume is console
            return true;
        if (Permission.TEMPLATE_BASE.hasPermission(player))
            return true;
        return Permission.hasPermission(player, Permission.TEMPLATE_BASE.getPath() + "." + template);
    }


    /**
     * Checks tag for invalid text
     * <i>Error messages are already handled</i>
     *
     * @return true if the tag is not allowed
     */
    private static boolean checkIllegalTagText(Object sender, String tag) {
        StringBuilder regex = new StringBuilder("^[&§");
        ConfigData.GENERAL_WHITELIST.get().forEach(regex::append);
        regex.append("]*$");
        Messages.DEBUG.sendConsole("Regex: " + regex);

        Matcher m = Pattern.compile(regex.toString()).matcher(tag);
        if (!m.matches()) {
            Messages.ERROR_CHAR.send(sender);
            return true;
        }

        List<String> rules = ConfigData.GENERAL_BLACKLIST.get();

        if (!Permission.BLACKLIST_BYPASS.hasPermission(sender) && !rules.isEmpty()) {
            Messages.DEBUG.sendConsole("Blacklist: '" + tag + "'");
            String strippedTag = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', tag));
            strippedTag = strippedTag.replaceAll(TagManager.getHexPattern(true), "");
            for (String rule : rules) {
                if (rule.isEmpty()) { //Pick out empty tags
                    if (strippedTag.isEmpty()) {
                        Messages.ERROR_BLACK.send(sender);
                        return true;
                    }
                    continue;
                }

                String compareTag = (rule.contains("&") || rule.contains("§")) ? tag : strippedTag;
                if (compareTag.toLowerCase().contains(rule.toLowerCase())) {
                    Messages.ERROR_BLACK.send(sender);
                    return true;
                }
            }
        }
        return false;
    }
}
