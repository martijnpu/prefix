package me.martijnpu.prefix.Util.Interfaces;

import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public abstract class IConfig {

    protected String locale = "en_US";

    public abstract boolean getBoolean(String path, boolean def);

    public abstract String getString(String path, String def);

    public abstract int getInt(String path, int def);

    public abstract List<String> getStringList(String path, List<String> def);

    public abstract Set<String> getConfigKeyList(String path, Set<String> def);

    public abstract boolean isSet(String path);

    public abstract void set(String path, Object value);

    public abstract void saveConfig();

    public abstract void reload();

    public abstract void loadLocale();

    /**
     * Get a list of all files in the plugin's resource folder.
     *
     * @return A list of file paths relative to the resource folder.
     */
    protected List<String> getResourceFiles() {
        List<String> fileNames = new ArrayList<>();
        String path = "languages/";

        try {
            Enumeration<URL> resources = getClass().getClassLoader().getResources(path);
            while (resources.hasMoreElements()) {
                URL resource = resources.nextElement();
                String decodedPath = URLDecoder.decode(resource.getPath(), StandardCharsets.UTF_8.name());
                String jarPath = decodedPath.substring(5, decodedPath.indexOf("!"));
                try (JarFile jarFile = new JarFile(jarPath)) {
                    Enumeration<JarEntry> entries = jarFile.entries();

                    while (entries.hasMoreElements()) {
                        JarEntry entry = entries.nextElement();
                        String entryName = entry.getName();
                        if (entryName.startsWith(path) && !entryName.equals(path) && !entry.isDirectory())
                            fileNames.add(entryName);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileNames;
    }
}
