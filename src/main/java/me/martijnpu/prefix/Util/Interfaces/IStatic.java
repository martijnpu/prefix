package me.martijnpu.prefix.Util.Interfaces;

import java.util.List;
import java.util.UUID;

public interface IStatic {
    String getServerVersion();

    List<String> getOnlinePlayers();

    String getDisplayName(Object player);

    UUID getUUID(Object sender);

    Object getPlayer(String name);
}
