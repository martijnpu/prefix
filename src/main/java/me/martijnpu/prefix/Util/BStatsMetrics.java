package me.martijnpu.prefix.Util;

import me.martijnpu.prefix.Bukkit.BukkitStatics;
import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.LuckPermsConnector;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.DrilldownPie;
import org.bstats.charts.SimplePie;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class BStatsMetrics {

    public static void addBukkitBStatsMeta(JavaPlugin plugin) {
        if (!ConfigData.BSTATS.get())
            return;

        Metrics metrics = new Metrics(plugin, 10920);
        metrics.addCustomChart(new SimplePie("luckpermsVersion", LuckPermsConnector::getLPVersion));
        metrics.addCustomChart(new SimplePie("language", ConfigData.LOCALE::get));
        if (Statics.currVersion >= Statics.newVersion)
            metrics.addCustomChart(new DrilldownPie("latestServerVersion", () -> new HashMap<String, Map<String, Integer>>() {{
                put(BukkitStatics.getMinecraftVersion(), new HashMap<String, Integer>() {{
                    put(Bukkit.getName(), 1);
                }});
            }}));
    }
}
