package me.martijnpu.prefix.Util.Tags;

import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Color;
import me.martijnpu.prefix.Util.LuckPermsResult;
import net.luckperms.api.context.ContextSet;

import java.util.regex.Matcher;

public class Tag {
    //It's possible to not have a prefix and only have a namecolor
    //Suffix doesn't have bracketcoloring
    //Possible to change colors and formatting independent
    // When no startChar, tagcolor is ignored

    public final ContextSet contextSet;
    public final String startChar;
    public final String endChar;
    public String startColor = "";
    public String endColor = "";
    public String tag = "";
    public String tagColor = "";

    Tag(LuckPermsResult result, String startChar, String endChar) {
        this.startChar = startChar;
        this.endChar = endChar;
        contextSet = result.contextSet;
        splitTag(result.tag);

        Messages.DEBUG.sendConsole("StartColor: '" + startColor.replace('&', '#') + "' --");
        Messages.DEBUG.sendConsole("Start-Char: '" + this.startChar.replace('&', '#') + "' --");
        Messages.DEBUG.sendConsole("TagColor: '" + tagColor.replace('&', '#') + "' --");
        Messages.DEBUG.sendConsole("Tag: '" + this.tag.replace('&', '#') + "' --");
        Messages.DEBUG.sendConsole("End-Char: '" + this.endChar.replace('&', '#') + "' --");
        Messages.DEBUG.sendConsole("EndColor: '" + endColor.replace('&', '#') + "' --");
    }

    public void changeStartColor(Color color, String hexadecimal) {
        startColor = changeColor(startColor, color, hexadecimal);
    }

    public void changeEndColor(Color color, String hexadecimal) {
        endColor = changeColor(endColor, color, hexadecimal);
    }

    private void changeTagColor(Color color, String hexadecimal) {
        tagColor = changeColor(tagColor, color, hexadecimal);
    }

    public void setColor(String color, boolean isPrefix) {
        if (ConfigData.PREFIX_BRACKET_ENABLED.get() && isPrefix)
            tagColor = color;
        else
            startColor = color;
    }

    public void changeColor(Color color, String hexColor, boolean isPrefix) {
        Messages.DEBUG.sendConsole("Changing " + (isPrefix ? "prefix" : "name") + " color");
        if (color != Color.HEXADECIMAL)
            hexColor = color.getColor();

        if (ConfigData.PREFIX_BRACKET_ENABLED.get() && isPrefix) {
            if (!ConfigData.PREFIX_BRACKET_COLOR.get().isEmpty())
                changeStartColor(Color.HEXADECIMAL, ConfigData.PREFIX_BRACKET_COLOR.get());
            changeTagColor(color, hexColor);
        } else
            changeStartColor(color, hexColor);
    }

    public String getNameColor(boolean isPrefix) {
        return ConfigData.PREFIX_BRACKET_ENABLED.get() && isPrefix ? tagColor : startColor;
    }

    public String getFullTag() {
        if (tag.isEmpty()) //Allows empty tags with only namecolors
            return endColor.isEmpty() ? startColor : endColor;

        boolean repeatStartColor = true;
        if (!ConfigData.PREFIX_BRACKET_ENABLED.get() && ConfigData.HEX_GRADIENT.get()) { // Include brackets into gradient
            Matcher match = TagManager.getTagSplitPattern().matcher(tag);
            if (match.matches() && !match.group(1).isEmpty() && !match.group(3).isEmpty()) {
                Messages.DEBUG.sendConsole("Tag not fully split. Trying again");
                startColor = match.group(1);
                tag = match.group(2);
                endColor = match.group(3);
                repeatStartColor = false;
            }
        }
        return startColor + startChar + tagColor + tag + (repeatStartColor ? startColor : "") + endChar + endColor;
    }

    private String changeColor(String currentColor, Color newColor, String hex) {
        String splitColor = "";
        String splitFormat = "";

        Matcher match = TagManager.getColorSplitPattern().matcher(currentColor);
        Messages.DEBUG.sendConsole("&bSplitting color " + currentColor.replaceAll("&", "%"));
        if (match.matches()) {
            if (match.group(1) != null)
                splitColor = match.group(1);
            if (match.group(2) != null)
                splitFormat = match.group(2);
        }

        if (newColor.isFormat) {
            if (splitFormat.contains(newColor.getColor()))
                splitFormat = splitFormat.replaceAll(newColor.getColor(), "");
            else
                splitFormat += newColor.getColor();
        } else
            splitColor = (newColor == Color.HEXADECIMAL ? hex : newColor.getColor());
        return splitColor + splitFormat;
    }

    private void splitTag(String fullTag) {
        if (fullTag.isEmpty())
            return;

        Matcher matcher = TagManager.getSplitPattern(startChar, endChar, true).matcher(fullTag);

        if (matcher.find()) {
            startColor = matcher.group(1);
            tagColor = matcher.group(3);
            tag = matcher.group(4);
            endColor = matcher.group(6);

            if (!startColor.isEmpty() && tag.endsWith(startColor)) {
                Messages.DEBUG.sendConsole("Found startColor before endBracket");
                tag = tag.substring(0, tag.length() - startColor.length());
            }
        }
    }
}
