package me.martijnpu.prefix.Util;

import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.FileHandler.Permission;
import me.martijnpu.prefix.Util.Interfaces.PrefixAdapter;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.UUID;

public class Statics extends PrefixAdapter {
    public static boolean debug = false;
    public static boolean isProxy;
    public static double newVersion;
    public static String newVersionString;
    public static double currVersion;
    public static String currVersionString;
    public static boolean isPAPIEnabled = false;

    public static void setCurrVersion(String pluginVersion) {
        if (pluginVersion.substring(pluginVersion.indexOf('.') + 1).length() == 1) //Change single version number: .1 -> .01
            pluginVersion = pluginVersion.replace(".", ".0");

        currVersionString = pluginVersion;
        currVersion = Double.parseDouble(pluginVersion);
    }

    public static void onPlayerJoin(Object player) {
        if ((newVersion > currVersion) && Permission.ADMIN.hasPermission(player)) {
            String text = "&eNew version of Prefix available!"
                    + "\nCurrent version: &f" + currVersionString
                    + "\n&eNew version: &f" + newVersionString;
            Messages.PLUGIN.sendBig(player, text);
        }
    }

    public static void checkForUpdate() {
        try {
            getSpigotVersion();
            if (newVersion > currVersion) {
                Messages.PLUGIN.sendBig(null, "" +
                        "&b------------------------------------------------\n" +
                        "&cA new version of Prefix is available.\n" +
                        "&bCurrent version: &a" + currVersionString + "&b, new version: &a" + newVersionString + "&b.\n" +
                        "&bURL: &ahttps://www.spigotmc.org/resources/prefix.70359/\n" +
                        "&b------------------------------------------------");
            } else if (newVersion < currVersion) {
                Messages.PLUGIN.sendConsole("&bYour version of PrefiX (v" + currVersionString + ") is ahead of the official release!");
            } else {
                Messages.PLUGIN.sendConsole("&aYour version of PrefiX (v" + currVersionString + ") is up to date!");
            }
        } catch (Exception e) {
            Messages.PLUGIN.sendBig(null, "" +
                    "&b------------------------------------------------\n" +
                    "&cUnable to receive version from Spigot.\n" +
                    "&cCurrent version: " + currVersionString + ".\n" +
                    "&cURL: &ahttps://www.spigotmc.org/resources/prefix.70359/\n" +
                    "&b------------------------------------------------");
            newVersion = 0;
        }
    }

    public static String getServerVersion() {
        return getStaticsAdapter().getServerVersion();
    }

    public static List<String> getOnlinePlayers() {
        return getStaticsAdapter().getOnlinePlayers();
    }

    public static String getDisplayName(Object player) {
        return getStaticsAdapter().getDisplayName(player);
    }

    public static UUID getUUID(Object sender) {
        return getStaticsAdapter().getUUID(sender);
    }

    public static Object getPlayer(String name) {
        return getStaticsAdapter().getPlayer(name);
    }

    private static void getSpigotVersion() throws IOException {
        final HttpsURLConnection connection = (HttpsURLConnection) new URL("https://api.spigotmc.org/legacy/update.php?resource=70359").openConnection();
        connection.setRequestMethod("GET");
        newVersionString = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();

        if (newVersionString.substring(newVersionString.indexOf('.') + 1).length() == 1) //Change single version number: .1 -> .01
            newVersionString = newVersionString.replace(".", ".0");
        newVersion = Double.parseDouble(newVersionString);
    }
}
