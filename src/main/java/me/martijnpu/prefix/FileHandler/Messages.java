package me.martijnpu.prefix.FileHandler;

import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.Util.Statics;
import me.martijnpu.prefix.Util.Tags.TagManager;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.Arrays;
import java.util.List;

import static me.martijnpu.prefix.Util.Interfaces.PrefixAdapter.getMessagesAdapter;

public enum Messages {
    //Custom, not translatable messages
    CUSTOM("custom-does-not-exist", "custom-does-not-exist"), //Only used while developing
    DEBUG("custom-does-not-exist", "custom-does-not-exist"), //Used for debug messages
    WARN("custom-does-not-exist", "custom-does-not-exist"), //Always check for config & permission
    PLUGIN("custom-does-not-exist", "custom-does-not-exist"), //Information from plugin
    LIST("custom-does-not-exist", "custom-does-not-exist"), //Used be the list command

    PREFIX("prefix"),
    LANGUAGE("language"),
    HEADER("messages.header"),
    FOOTER("messages.footer"),
    CMD_UNKNOWN("messages.command.unknown", "%COMMAND%"),
    CMD_USAGE("messages.command.usage", "%COMMAND%"),
    CMD_RELOAD("messages.command.reload"),
    CMD_RESET("messages.command.reset", "%TYPE%"),
    CMD_RESET_OTHER("messages.command.reset-other", "%TYPE%", "%NAME%"),
    CMD_CHANGED("messages.command.changed", "%TYPE%", "%TAG%"),
    CMD_CHANGED_OTHER("messages.command.changed-other", "%TYPE%", "%NAME%"),
    CMD_REMOVED("messages.command.removed", "%TYPE%"),
    CMD_REMOVED_OTHER("messages.command.removed", "%TYPE%", "%NAME%"),
    COLOR_OTHER("messages.color.target", "%TYPE%", "%NAME%"),
    COLOR_RESET("messages.color.reset", "%TYPE%"),
    COLOR_APPLY("messages.color.apply", "%TYPE%", "%COLOR%"),
    COLOR_INVALID("messages.color.invalid"),
    LIST_APPLY("messages.list.apply", "%TYPE%"),
    LIST_NO_PERM("messages.list.no-perm", "%TYPE%"),
    ERROR_PLAYER("messages.error.player", "%NAME%"),
    ERROR_COMMAND("messages.error.command"),
    ERROR_TARGET_NEED("messages.error.need-target"),
    ERROR_BLACK("messages.error.blacklist"),
    ERROR_LENGTH("messages.error.length", "%SIZE%"),
    ERROR_COLOR_LENGTH("messages.error.length-colorcode", "%SIZE%"),
    ERROR_CHAR("messages.error.character"),
    ERROR_COLOR("messages.error.colorcode"),
    ERROR_HEX("messages.error.hexadecimal"),
    ERROR_BRACKET("messages.error.bracket"),
    NO_PERM_CMD("messages.error.permission.command"),
    NO_PERM_COLOR("messages.error.permission.color"),
    NO_PERMS_TARGET("messages.error.permission.target"),
    HELP("messages.help"),
    HELP_OTHERS("messages.help-others"),
    TEMPLATE_DISABLED("messages.error.template.disabled"),
    TEMPLATE_UNKNOWN("messages.error.template.unknown"),
    TEMPLATE_LISTHEADER("messages.list.template-header");

    private final String path;
    private final List<String> placeholders;

    Messages(String path, String... placeholders) {
        this.path = path;
        this.placeholders = Arrays.asList(placeholders);
    }

    public String getPath() {
        return path;
    }

    public void sendBig(Object sender, TextComponent text) {
        TextComponent newLine = new TextComponent("\n");
        TextComponent message = new TextComponent();

        message.addExtra(newLine);
        message.addExtra(new TextComponent(HEADER.convert()));
        message.addExtra(newLine);
        message.addExtra(text);
        if (!text.getText().endsWith("\n"))
            message.addExtra(newLine);
        message.addExtra(new TextComponent(FOOTER.convert()));

        getMessagesAdapter().sendBigMessage(sender, message);

    }

    public void sendBig(Object sender, String... replacements) {
        sendBig(sender, new TextComponent(convert(replacements)));
    }

    /**
     * Send to player
     */
    public void send(Object sender, String... replacements) {
        if (this == DEBUG && !ConfigData.DEBUG.get())
            return;
        if (this == WARN && ConfigData.HIDE_WARNING.get() && !Permission.ADMIN.hasPermission(sender))
            return;

        getMessagesAdapter().sendMessage(sender, getReplacedMessage(replacements));
    }

    /**
     * Send to console
     */
    public void sendConsole(String... replacements) {
        String prefix = Statics.isProxy ? "" : "&6[PrefiX] &r";
        if (this == DEBUG) {
            if (ConfigData.DEBUG.get())
                prefix += "&4[DEBUG]&c ";
            else
                return;
        }

        if (this == WARN)
            getMessagesAdapter().sendConsoleWarning(getReplacedMessage(replacements));
        else
            getMessagesAdapter().sendConsole(prefix + getReplacedMessage(replacements));
    }

    public BaseComponent[] convert(String... replacements) {
        return getMessagesAdapter().convert(getReplacedMessage(replacements));
    }

    private String getReplacedMessage(String... replacements) {
        String message = getMessagesAdapter().getMessage(path);
        if (replacements.length > placeholders.size())
            WARN.sendConsole("There is a placeholder missing in key '" + path + "'. Check the documentation or report this to the developer!");
        if (replacements.length < placeholders.size())
            WARN.sendConsole("There are too many placeholder found in key '" + path + "'. Check the documentation or report this to the developer!");
        for (int i = 0; i < placeholders.size(); i++)
            message = message.replace(placeholders.get(i), replacements[i]);
        return message;
    }

    /**
     * Get the full Tag with adapted hexadecimal colors for the chat.
     * None colors if the MC version < 16. Else chat hexadecimal colors</>
     *
     * @return Formatted tag
     */
    public static String convertHexadecimalToChat(String input) {
        String[] versions = new String[]{"1.8.", "1.9.", "1.10.", "1.11.", "1.12.", "1.13.", "1.14.", "1.15."};
        if (Arrays.stream(versions).anyMatch(Statics.getServerVersion()::contains))
            return input.replaceAll(TagManager.getHexPattern(true), "&f");
        return TagManager.convertHexadecimal(input);
    }
}
