package me.martijnpu.prefix.FileHandler.Config;

import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Statics;
import me.martijnpu.prefix.Util.Tags.TagManager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static me.martijnpu.prefix.FileHandler.Config.ConfigKey.hide;
import static me.martijnpu.prefix.FileHandler.Config.ConfigKeyAdapter.*;
import static me.martijnpu.prefix.Util.Interfaces.PrefixAdapter.getConfigAdapter;

public class ConfigData {
    public static final ConfigKey<Boolean> DEBUG = hide(booleanKey("debug", false));
    public static final ConfigKey<Boolean> BSTATS = hide(booleanKey("bstats", true));
    public static final ConfigKey<Integer> PREFIX_MAX_LENGTH = integerKey("prefix.max-length", 16);
    public static final ConfigKey<String> PREFIX_START_CHAR = stringKey("prefix.start-character", "[");
    public static final ConfigKey<String> PREFIX_END_CHAR = stringKey("prefix.end-character", "] ");
    public static final ConfigKey<Boolean> PREFIX_BRACKET_ENABLED = booleanKey("prefix.bracket.enable", false);
    public static final ConfigKey<String> PREFIX_BRACKET_COLOR = stringKey("prefix.bracket.change-color", "");
    public static final ConfigKey<Integer> SUFFIX_MAX_LENGTH = integerKey("suffix.max-length", 8);
    public static final ConfigKey<String> SUFFIX_START_CHAR = stringKey("suffix.start-character", " <");
    public static final ConfigKey<String> SUFFIX_END_CHAR = stringKey("suffix.end-character", ">");
    public static final ConfigKey<Boolean> HEX_ENABLED = booleanKey("general.hexadecimal.enabled", false);
    public static final ConfigKey<String> HEX_FORMAT = stringKey("general.hexadecimal.format", "&#rrggbb");
    public static final ConfigKey<Boolean> HEX_GRADIENT = booleanKey("general.hexadecimal.gradient", false);
    public static final ConfigKey<Boolean> HEX_NAMES = booleanKey("general.hexadecimal.hex-names", false);
    public static final ConfigKey<Boolean> HIDE_WARNING = booleanKey("general.hide-warnings", false);
    public static final ConfigKey<List<String>> GENERAL_WHITELIST = stringListKey("general.whitelist", new String[]{"\\w", "\\p{Punct}", " "});
    public static final ConfigKey<List<String>> GENERAL_BLACKLIST = stringListKey("general.blacklist", new String[]{"fuck", "admin", "owner"});
    public static final ConfigKey<String> LOCALE = stringKey("general.language", "en_US");
    public static final ConfigKey<Boolean> KEEP_CONTEXT = booleanKey("general.keep-context", false);
    public static final ConfigKey<Boolean> TEMPLATE_ENABLED = booleanKey("templates.enabled", false);
    public static final ConfigKey<Set<String>> TEMPLATE_LIST = hide(configListKey("templates.list"));
    public static final ConfigKey<String> TEMPLATE_EXAMPLE = hide(stringKey("templates.list.example", "&a[&bTemplateExample&a] "));

    private static final ConfigData instance = new ConfigData();
    private static List<ConfigKey<?>> KEYS;

    private ConfigData() {
        KEYS = new ArrayList<>();

        try {
            for (Field declaredField : getClass().getDeclaredFields())
                if (declaredField.getType().equals(ConfigKey.class))
                    KEYS.add((ConfigKey<?>) declaredField.get(this));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        reload(null);
    }

    public static ConfigData getInstance() {
        return instance;
    }

    public void printInitData() {
        Messages.PLUGIN.sendConsole("&aLoaded " + KEYS.size() + " config values");
    }

    public String getCustomString(String path, String def) {
        return stringKey(path, def).get();
    }

    public void reload(Object sender) {
        getConfigAdapter().reload();

        boolean save = false;
        for (ConfigKey<?> key : KEYS)
            save |= key.setDefault(false);
        save |= checkForValidData();

        if (save)
            getConfigAdapter().saveConfig();
        checkForDebug();

        getConfigAdapter().loadLocale();

        if (save)
            Messages.ERROR_COMMAND.send(sender);
        else if (instance != null) //First time
            Messages.CMD_RELOAD.send(sender);
    }

    private void checkForDebug() {
        Statics.debug = DEBUG.get();
        Messages.DEBUG.sendConsole("&4Debugmodus enabled. Be aware for spam!");
    }

    /**
     * Check config for invalid fields
     *
     * @return Config needs to be saved
     */
    private boolean checkForValidData() {
        boolean save = false;
        if (!HEX_FORMAT.get().contains("rrggbb")) {
            Messages.WARN.sendConsole("&4Config value \"Hexadecimal format\" doesn't contain required \"" + TagManager.hexReplacement + "\". Resetting to default value...");
            HEX_FORMAT.setDefault(true);
            save = true;
        }

        if (TEMPLATE_LIST.get().isEmpty()) {
            Messages.WARN.sendConsole("&cThe config option \"templates.list\" didn't contain any items. Generating an example...");
            TEMPLATE_EXAMPLE.setDefault(true);
            save = true;
        }
        return save;
    }
}
