package me.martijnpu.prefix.FileHandler;

import me.martijnpu.prefix.Util.Statics;
import org.jetbrains.annotations.Nullable;

public enum Permission {
    TARGET_OTHER("prefix.other"),
    ADMIN("prefix.admin"),
    PREFIX_CHAR("prefix.char"),
    SUFFIX_CHAR("suffix.char"),
    PREFIX_CHANGE("prefix.change"),
    SUFFIX_CHANGE("suffix.change"),
    PREFIX_RESET("prefix.reset"),
    SUFFIX_RESET("suffix.reset"),
    PREFIX_LIST("prefix.list"),
    SUFFIX_LIST("suffix.list"),
    SUFFIX_REMOVE("suffix.remove"),
    BLACKLIST_BYPASS("prefix.blacklist"),
    COLOR_BASE("prefix.color"),
    COLOR_BASE_PREFIX("prefix.color.prefix"),
    COLOR_BASE_NAME("prefix.color.name"),
    COLOR_BASE_BRACKET("prefix.color.bracket"),
    COLOR_BASE_SUFFIX("suffix.color"),
    TEMPLATE_BASE("prefix.template"),
    TEMPLATE_LIST("prefix.templatelist");

    private final String path;

    Permission(String path) {
        this.path = path;
    }

    /**
     * Checks if the sender has a custom permission
     */
    public static boolean hasPermission(@Nullable Object sender, String path) {
        if (sender == null)
            return true;

        if (Statics.isProxy)
            return ((net.md_5.bungee.api.CommandSender) sender).hasPermission(path);
        else
            return ((org.bukkit.command.CommandSender) sender).hasPermission(path);
    }

    /**
     * Checks if the sender has the permission
     */
    public boolean hasPermission(@Nullable Object sender) {
        if (sender == null)
            return true;
        return hasPermission(sender, path);
    }

    /**
     * Checks perm and sends the "NO_PERM_CMD" message to the @sender
     */
    public boolean hasPermissionMessage(Object sender) {
        boolean perm = hasPermission(sender);
        if (!perm)
            Messages.NO_PERM_CMD.send(sender);
        return perm;
    }

    public String getPath() {
        return path;
    }
}
