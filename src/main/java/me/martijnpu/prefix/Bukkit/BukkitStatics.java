package me.martijnpu.prefix.Bukkit;

import me.martijnpu.prefix.Util.Interfaces.IStatic;
import me.martijnpu.prefix.Util.Statics;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class BukkitStatics implements IStatic {
    private static final BukkitStatics instance = new BukkitStatics();

    private BukkitStatics() {
    }

    public static BukkitStatics getInstance() {
        return instance;
    }

    public static String getMinecraftVersion() {
        // Same substring as the one bStats uses, so should be safe
        String version = Bukkit.getVersion();
        int start = version.indexOf("MC: ") + 4;
        int end = version.length() - 1;
        return version.substring(start, end);
    }

    /**
     * @return Returns the sub version: 1.x.x
     */
    public static double getSubVersion() {
        String version = BukkitStatics.getMinecraftVersion();
        version = version.substring(version.indexOf(".") + 1);
        return Double.parseDouble(version);
    }

    public static void scheduleUpdateChecker(Plugin plugin) {
        Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, Statics::checkForUpdate, 0L, 20L * 60L * 60L * 24L);
    }

    @Override
    public String getServerVersion() {
        return Bukkit.getVersion();
    }

    @Override
    public List<String> getOnlinePlayers() {
        return Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList());
    }

    @Override
    public String getDisplayName(Object player) {
        if (player instanceof Player)
            return ((Player) player).getDisplayName();
        else
            return "Console";
    }

    @Override
    public UUID getUUID(Object sender) {
        if (sender instanceof Player)
            return ((Player) sender).getUniqueId();
        else
            return null;
    }

    @Override
    public Object getPlayer(String name) {
        return Bukkit.getPlayer(name);
    }
}
